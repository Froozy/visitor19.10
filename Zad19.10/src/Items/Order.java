package Items;

import java.sql.Date;
import java.util.List;

import Clients.Client;
import Clients.Visitable;
import Repost.Visitor;

public class Order implements Visitable {

	String Number;
	double OrderTotalPrice;
	Date OrderDate;
	List<Product> ProductsList;
	
	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public String giveReport() {
		return this.toString();
	}

	public String getNumber() {
		return Number;
	}

	public double getOrderTotalPrice() {
		return OrderTotalPrice;
	}

	public Date getOrderDate() {
		return OrderDate;
	}
	public void addProduct(Product product)
	{
		this.ProductsList.add(product);
		this.OrderTotalPrice += product.price;
		
	}
	String ReadAll()
	{
		String temp = null;
		System.out.println("Products from order #"+this.Number+": ");
		for(Product x : this.ProductsList)
		{
			temp += x.name + "\n";
		}
		return temp;
	}
	

}
