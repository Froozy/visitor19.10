package Items;

import Clients.Visitable;
import Repost.Visitor;

public class Product implements Visitable {

	String name;
	double price;
	
	public Product(String n,double p)
	{
		name = n;
		price = p;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public String giveReport() {
		return this.name;
	}
}
