package Clients;

import java.util.List;

import Repost.Visitor;
import Items.Order;

public class Client implements Visitable {
	String number;
	List<Order> orders;
	
	
	void setNumber(String number)
	{
		this.number = number;
	}
	String getNumber()
	{
		return number;
	}
	public void addOrder(Order order)
	{
		orders.add(order);
	}
	
	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public String giveReport() {
		return this.ReadAll();
	}
	String ReadAll()
	{
		String temp = null;
		System.out.println("This client orders: ");
		for(Order x : this.orders)
		{
			temp += x.getNumber() + "\n";
		}
		return temp;
	}

}
