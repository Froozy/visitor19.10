
package Clients;

import java.util.List;

import Repost.Visitor;

public class ClientGroup implements Visitable {
	String memberName;
	List<Client> clientList;

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public String giveReport() 
	{	
		return this.ReadAll();
	}

	public void addClient(Client client)
	{
		this.clientList.add(client);
	}
	String ReadAll()
	{
		String temp = null;
		System.out.println("Members of "+this.memberName+": ");
		for(Client x : this.clientList)
		{
			temp += x.number + "\n";
		}
		return temp;
	}
}
