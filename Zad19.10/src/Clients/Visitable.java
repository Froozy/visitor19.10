package Clients;

import Repost.Visitor;

public interface Visitable {
	
	void accept(Visitor visitor);
	public String giveReport();

}
