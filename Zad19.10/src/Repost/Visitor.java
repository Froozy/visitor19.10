package Repost;

import Clients.Visitable;

public interface Visitor {
	void visit(Visitable v);

}
