package Repost;

import Clients.Visitable;

public class RepostMaker implements Visitor {
	int numberofClients;
	int numberofOrders;
	int	numberofProducts;
	String report = null;
	
	public int getNumberofClients() {
		return numberofClients;
	}

	public int getNumberofOrders() {
		return numberofOrders;
	}

	public int getNumberofProducts() {
		return numberofProducts;
	}
	@Override
	public void visit(Visitable v) {
			report=report+v.giveReport();
        }
	
	String ShowRepost()
	{
		return this.report;
	}
}
